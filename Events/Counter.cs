﻿using UnityEngine;

/// <summary>
/// считалка на эвентах
/// </summary>
public class Counter : MonoBehaviour
{
    [SerializeField] private int maxCount;
    [SerializeField] private int startCount;
    [SerializeField] private bool resetOnReach;

    [SerializeField] private IntEvent OnCountReached;
    [SerializeField] private IntEvent OnCountNotReached;
    [SerializeField] private IntEvent OnChange;

    public int Count { get; set; }
    private bool countReached;
    private int previousCount;


    private void Start()
    {
        Reset();
    }

    public void Add(int value)
    {
        Count = Mathf.Clamp(Count + value, startCount, maxCount);
        CheckCount();
    }

    public void Subtract(int value)
    {
        Count = Mathf.Clamp(Count - value, startCount, maxCount);
        CheckCount();
    }

    void CheckCount()
    {
        if (Count != previousCount)
        {
            OnChange.Invoke(Count);
            previousCount = Count;
        }

        if (Count == maxCount)
        {
            if (!countReached)
            {
                countReached = true;
                OnCountReached.Invoke(Count);
                if (resetOnReach)
                {
                    Reset();
                }
            }
        }
        else
        {
            OnCountNotReached.Invoke(Count);
        }
    }

    public void Reset()
    {
        countReached = false;
        Count = startCount;
        OnChange.Invoke(Count);
    }
}
