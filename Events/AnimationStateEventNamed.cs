﻿using UnityEngine;

/// <summary>
/// Позволяет в аниматоре отлавливать события начала и конца стейта
///
/// На объекте с Аниматором необходим компонент EventManagerNamed
/// </summary>
public class AnimationStateEventNamed : StateMachineBehaviour
{
    private EventManagerNamed _eventManagerNamed;
    public string enterEventName = "";
    public string exitEventName = "";


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_eventManagerNamed)
        {
            _eventManagerNamed = animator.gameObject.GetComponent<EventManagerNamed>();
        }

        if (_eventManagerNamed)
        {
            if (!string.IsNullOrEmpty(enterEventName))
                _eventManagerNamed.RunEvent(enterEventName);
        }

    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_eventManagerNamed)
        {
            _eventManagerNamed = animator.gameObject.GetComponent<EventManagerNamed>();
        }

        if (_eventManagerNamed)
        {
            if (!string.IsNullOrEmpty(exitEventName))
                _eventManagerNamed.RunEvent(exitEventName);
        }
    }
}
