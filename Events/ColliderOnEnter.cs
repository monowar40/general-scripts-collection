﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// UnityEvent обертка над стандартным OnTriggerEnter
/// </summary>

public class ColliderOnEnter : MonoBehaviour
{
    [SerializeField] private GameObjectEvent onEnter;
    [SerializeField] private string requiredComponent = "";
    [SerializeField] private bool destroyOnEnter;

    private void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!enabled)
        {
            return;
        }

        if (requiredComponent != "")
        {
            if(!other.GetComponent(requiredComponent))
            {
                return;
            }
        }

        onEnter?.Invoke(other.gameObject);

        if (destroyOnEnter)
        {
            Destroy(gameObject);
        }
    }
}
