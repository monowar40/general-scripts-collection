using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Позволяет запускать/перезапускать unity сцены через эвенты
/// </summary>
public class SceneLoader : MonoBehaviour
{
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
}
