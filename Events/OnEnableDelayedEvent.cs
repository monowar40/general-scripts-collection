﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// Обычный UnityEvent, который пнется через delay после OnEnable'a
/// </summary>
public class OnEnableDelayedEvent : MonoBehaviour {

	[SerializeField] private float delay = 0.5f;
	[SerializeField] private bool disableGameobjectAfterEvent = true;
	[SerializeField] private UnityEvent enableEvent;

	void OnEnable ()
	{
		StartCoroutine(RunEvent());
	}
	private IEnumerator RunEvent ()
	{
		yield return new WaitForSeconds(delay);
		enableEvent?.Invoke();
		if (disableGameobjectAfterEvent)
		{
			gameObject.SetActive(false);
		}
	}
}

