﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

/// <summary>
/// Класс позволяющий пинать случайные эвенты из Листа,
/// notTooRandom - Позволяет избегать вызов одного и того же эвента
/// UseUnityRandom - переключение unity Random или C# Random
/// shakeOnEnable - перемешивать ли лист в OnEnable
/// shakeAfterReached - перемешивать ли лист после всех эвентов
/// </summary>
[Serializable]
public class RandomEvent
{
    public UnityEvent Event;
}

public class RandomEvents : MonoBehaviour
{
    [SerializeField] private bool notTooRandom = false;
    [SerializeField] private bool useUnityRandom = false;
    [SerializeField] private bool shakeOnEnable = true;
    [SerializeField] private bool shakeAfterReached = true;
    [SerializeField] private List<RandomEvent> randomEventsList;
    
    private int _curEvent = 0;

    private void OnEnable()
    {
        if (shakeOnEnable)
        {
            ShakeEvents();
        }
    }

    public void ShakeEvents()
    {
        if (useUnityRandom)
        {
            for (int i = 0; i < randomEventsList.Count; i++)
            {
                RandomEvent tmp = randomEventsList[0];
                randomEventsList.RemoveAt(0);
                randomEventsList.Insert(Random.Range(0, randomEventsList.Count + 1), tmp);
            }
        }
        else
        {
            System.Random rnd = new System.Random();
            for (int i = 0; i < randomEventsList.Count + 1; i++)
            {
                RandomEvent tmp = randomEventsList[0];
                randomEventsList.RemoveAt(0);
                randomEventsList.Insert(rnd.Next(randomEventsList.Count + 1), tmp);
            }
        }
    }

    private int NumberEvent()
    {
        if (_curEvent < randomEventsList.Count)
        {
            return _curEvent;
        }
        else
        {
            _curEvent = 0;
            if (shakeAfterReached)
            {
                ShakeEvents();
            }

            return _curEvent;
        }

    }

    public void InvokeRandomEvent()
    {
        if (randomEventsList == null)
            return;
        if (notTooRandom)
        {
            randomEventsList[NumberEvent()].Event.Invoke();
            _curEvent++;
        }
        else
        {
            randomEventsList[Random.Range(0, randomEventsList.Count)].Event.Invoke();
        }
    }

}
