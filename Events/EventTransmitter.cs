﻿using UnityEngine;

/// <summary>
/// Позвволяет отправлять "сигналы" о наступлении какого-либо события для реакции на него в другом месте в EventReceiver'е,
/// избавившись от прямых ссылок
///
/// TODO: переписать на делегатах, избавившись от FindObjectsOfType
/// </summary>
public class EventTransmitter : MonoBehaviour
{
    [SerializeField] private string eventTag = "";

    public void TransmitEvent()
    {
        if (string.IsNullOrEmpty(eventTag))
        {
            return;
        }

        var receivers = FindObjectsOfType<EventReceiver>();

        for (int i = 0; i < receivers.Length; i++)
        {
            var receiver = receivers[i];

            receiver.TryInvoke(eventTag);
        }
    }
}