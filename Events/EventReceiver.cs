﻿using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Реагирует на EventTransmitter
///
/// TODO: переписать на делегатах 
/// </summary>
    public class EventReceiver : MonoBehaviour
    {
        [SerializeField] private string myEventTag = "";
        [SerializeField] private UnityEvent eventOnReceive;

        public void TryInvoke(string eventTag)
        {
            if (eventTag == myEventTag)
            {
                eventOnReceive?.Invoke();
            }
        }
    }

