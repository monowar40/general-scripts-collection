﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Обычный UnityEvent, который сколько не пинай, выполниться только 1 раз
/// но если очень нужно, можно вызвать ResetOnceEvent() и вызвать еще раз(либо вызвать ForceInvokeEvent() и принудительно вызвать)
/// </summary>
public class OnlyOnceEvent : MonoBehaviour
{
	[SerializeField] private bool InvokeOnEnable;
	[SerializeField] private UnityEvent OnceEvent;
	private bool isInvoked = false;
	
	void Start()
	{
		if(InvokeOnEnable)
			InvokeEvent();
	}

	public void InvokeEvent()
	{
		if (isInvoked == false)
		{
			isInvoked = true;
			if(OnceEvent != null)
				OnceEvent.Invoke();
		}
	}

	public void ForceInvokeEvent()
	{
		if(OnceEvent != null)
			OnceEvent.Invoke();
	}

	public void ResetOnceEvent()
	{
		isInvoked = false;
	}
}
