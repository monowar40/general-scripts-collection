﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

/// <summary>
/// Позволяет вызывать unityEvent'ы по названию
/// </summary>
[Serializable]
public class NamedEvent
{
    public string name;
    public UnityEvent unityEvent;
}

public class EventManagerNamed : MonoBehaviour
{
    public List<NamedEvent> namedEventsEvents;

    public void RunEvent(string eventName)
    {
        for (int i = 0; i < namedEventsEvents.Count; i++)
        {
            if (namedEventsEvents[i].name == eventName)
            {
                namedEventsEvents[i].unityEvent?.Invoke();
                return;
            }

        }

        Debug.LogWarning(gameObject.name + ": 404 Event: " + eventName + " Not Found");
    }

}
