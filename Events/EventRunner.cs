﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Очередь из эвентов
/// Parallel - отсчитывает задержку от старта
/// Sequence - отсчитывает задержку от предыдущего
/// </summary>

public enum RunnerType
{
	Parallel,
	Sequence
}

[Serializable]
public class DelayedEvent
{
	public float delay = 0.01f;
	public UnityEvent Event;
}
public class EventRunner : MonoBehaviour
{
	[SerializeField] private RunnerType runnerType;
	[SerializeField] private bool autoStartOnEnable = false;
	[SerializeField] private List<DelayedEvent> eventsList;

	private void OnEnable()
	{
		if (autoStartOnEnable)
			StartEvents();
	}

	public void StartEvents()
	{
		switch (runnerType)
		{
			case RunnerType.Parallel:
				foreach (DelayedEvent parallelEvent in eventsList)
				{
					StartCoroutine(ParallelDelayedEventsInvoking(parallelEvent));
				}
				break;
			case RunnerType.Sequence:
				StartCoroutine(SequenceDelayedEventsInvoking());
				break;
		}
		
	}

	public void StopAllEvents()
	{
		StopAllCoroutines();
	}

	private IEnumerator SequenceDelayedEventsInvoking()
	{
		foreach (DelayedEvent sEvent in eventsList)
		{
			yield return new WaitForSeconds(sEvent.delay);

			sEvent.Event?.Invoke();
		}
	}

	private IEnumerator ParallelDelayedEventsInvoking(DelayedEvent delayedEvent)
	{
		yield return new WaitForSeconds(delayedEvent.delay);
		delayedEvent.Event?.Invoke();
		
	}

	private void OnDestroy()
	{
		StopAllEvents();
	}
}
