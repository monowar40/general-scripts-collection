using System.Collections;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private bool followX;
    [SerializeField] private bool followY;
    [SerializeField] private bool followZ;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float followSpeed = 1;
    
    private Transform _myTransform;

    private void OnEnable()
    {
        _myTransform = transform;
        _myTransform.position = target.position + offset;

        StartCoroutine(Follow());
    }

    private IEnumerator Follow()
    {
        while (enabled)
        {
            yield return new WaitForEndOfFrame();
            
            var destinationPosition = target.position + offset;
            var position = _myTransform.position;
            var finalVec = new Vector3(position.x, position.y, position.z);
            if (followX)
            {
                finalVec.x = destinationPosition.x;
            }
            if (followY)
            {
                finalVec.y = destinationPosition.y;
            }
            if (followZ)
            {
                finalVec.z = destinationPosition.z;
            }

            _myTransform.position = Vector3.Lerp(_myTransform.position, finalVec, Time.deltaTime*followSpeed); 
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
