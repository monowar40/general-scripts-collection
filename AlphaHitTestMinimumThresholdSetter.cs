﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AlphaHitTestMinimumThresholdSetter : MonoBehaviour
{
    [SerializeField]
    private float alphaHitTestMinimumThreshold = 0.5f;


    void Start()
    {
        GetComponent<Image>().alphaHitTestMinimumThreshold = alphaHitTestMinimumThreshold;
    }
}
