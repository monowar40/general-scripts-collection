﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Реализация простейшего "твина" на корутине
/// двигает объекты от своих координат в необходимые
/// </summary>
public enum TransformMetod
{
    Slerp,
    Lerp
}
public class TweenOnCoroutine : MonoBehaviour
{
    [SerializeField] private Transform transformToTween;
    [SerializeField] private TransformMetod _transformMetod = TransformMetod.Lerp;
    [SerializeField] private float timeToTween = 1f;
    [SerializeField] private bool tweenPosition = true;
    [SerializeField] private bool tweenRotation = true;
    [SerializeField] private bool tweenScale = true;
    [SerializeField] private UnityEvent OnComplete;

    private Vector3 _startPosition, _startScale;
    private Quaternion _startRotation;

    private void Start()
    {
        if (transformToTween == null)
            transformToTween = transform;

        InitStartTransform();
    }
    public void InitStartTransform()
    {
        _startPosition = transformToTween.position;
        _startRotation = transformToTween.rotation;
        _startScale = transformToTween.localScale;
    }
    public void ResetToStartTransform()
    {
        transformToTween.position = _startPosition;
        transformToTween.rotation = _startRotation;
        transformToTween.localScale = _startScale;
    }
    public void TweenToStartTransform()
    {
        StartCoroutine(TweenTransform(_startPosition, _startRotation, _startScale, timeToTween));
    }
    public void GoToDestination(Transform destinationTransform)
    {
        if (destinationTransform != null)
        {
            var destinationPos = destinationTransform.position;
            var destinationRot = destinationTransform.rotation;
            var destinationScale = destinationTransform.localScale;
            StartCoroutine(TweenTransform(destinationPos, destinationRot, destinationScale, timeToTween));
        }
    }
    public void SetTime(float time)
    {
        timeToTween = time;
    }
    private IEnumerator TweenTransform(Vector3 pos, Quaternion rot, Vector3 scale, float time)
    {
        float elapsedTime = 0;

        var startPos = transformToTween.position;
        var startRot = transformToTween.rotation;
        var startScale = transformToTween.localScale;
        while (elapsedTime < time)
        {
            if (tweenPosition)
            {
                switch (_transformMetod)
                {
                    case TransformMetod.Slerp:
                        transformToTween.position = Vector3.Slerp(startPos, pos, (elapsedTime / time));
                        break;
                    case TransformMetod.Lerp:
                        transformToTween.position = Vector3.Lerp(startPos, pos, (elapsedTime / time));
                        break;
                }
                
            }

            if (tweenRotation)
            {
                switch (_transformMetod)
                {
                    case TransformMetod.Slerp:
                        transformToTween.rotation = Quaternion.Slerp(startRot, rot, (elapsedTime / time));
                        break;
                    case TransformMetod.Lerp:
                        transformToTween.rotation = Quaternion.Lerp(startRot, rot, (elapsedTime / time));
                        break;

                }
                
            }
            if (tweenScale)
            {
                switch (_transformMetod)
                {
                    case TransformMetod.Slerp:
                        transformToTween.localScale = Vector3.Slerp(startScale, scale, (elapsedTime / time));

                        break;
                    case TransformMetod.Lerp:
                        transformToTween.localScale = Vector3.Lerp(startScale, scale, (elapsedTime / time));

                        break;

                }
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (tweenPosition)
        {
            transformToTween.position = pos;
        }
        if (tweenRotation)
        {
            transformToTween.rotation = rot;
        }
        if (tweenScale)
        {
            transformToTween.localScale = scale;
        }

        OnComplete?.Invoke();
    }
}
