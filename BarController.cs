using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Управляет любыми барами(полоски здоровья-маны-таймеры) через изменение fillAmount
/// </summary>
public class BarController : MonoBehaviour
{
   [SerializeField] private float startValue;
   [SerializeField] private float maxValue;
   [SerializeField] private Image barImage;
   
   [SerializeField] private UnityEvent onChange;
   [SerializeField] private UnityEvent onZeroValueReached;
   [SerializeField] private UnityEvent onMaxValueReached;
   
   private float _curValue;

   private void Start()
   {
      _curValue = startValue;
   }
   
   public void ChangeValue(float value)
   {
      _curValue += value;
      onChange?.Invoke();
      
      if (_curValue <= 0)
      {
         _curValue = 0;
         if(barImage) barImage.fillAmount = 0;
         
         onZeroValueReached?.Invoke();
      }
      else if (_curValue > 0 && _curValue < maxValue)
      {
         if(barImage) barImage.fillAmount = _curValue / maxValue;
      }
      else if (_curValue >= maxValue)
      {
         _curValue = maxValue;
         if(barImage) barImage.fillAmount = 1;
         
         onMaxValueReached?.Invoke();
      }
   }
}
