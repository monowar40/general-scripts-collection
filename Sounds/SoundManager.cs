﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Менеджер звуков.
/// </summary>

[System.Serializable]
public class Sound {

	public string name;
	public AudioClip clip;

	public float masterVolume = 1.0f;
	public float masterPitch = 1.0f;

	[Range(0.0f, 1.0f)]
	public float volume = 1.0f;
	[Range(0.5f, 1.5f)]
	public float pitch = 1.0f;

	[Range(0.0f, 0.5f)]
	public float randomVolume = 0.1f;
	[Range(0.0f, 0.5f)]
	public float randomPitch = 0.1f;

	public bool loop = false;

	private AudioSource source;

	public void SetSource (AudioSource _source)
	{
		source = _source;
		source.clip = clip;
		source.loop = loop;
		source.playOnAwake = false;
	}

	public void Play ()
	{
		if (source) {
			source.volume = volume * (1 + Random.Range (-randomVolume / 2.0f, randomVolume / 2.0f)) * masterVolume;
			source.pitch = pitch * (1 + Random.Range (-randomPitch / 2.0f, randomPitch / 2.0f)) * masterPitch;
			source.Play ();
		}
	}

	public void Pause()
	{
		if (source)
		{
			source.Pause();
		}
	}

	public void Stop ()
	{
        if (source)
        {
            source.Stop();
        }
	}

	public void Mute ()
	{
        if (source)
        {
            source.mute = true;
        }
	}

	public void Unmute ()
	{
        if (source)
        {
            source.mute = false;
        }
	}

	public float GetSoundLenght()
	{
		float tmp = new float();
		if (source)
		{
			tmp = source.clip.length;
		}
		return tmp;
	}
	public float GetSoundCurPosition()
	{
		float tmp = new float();
		if (source)
		{
			tmp = source.time;
		}
		return tmp;
	}
}

[ExecuteInEditMode]
public class SoundManager : MonoBehaviour {

	[SerializeField] private Sound[] sounds;
	[SerializeField] private AudioClip[] clips;

	void Awake ()
	{
		if (Application.isPlaying) {
			for (int i = 0; i < sounds.Length; i++) {
				GameObject soundObject = new GameObject ("Sound_" + i + "_" + sounds [i].name);
				sounds [i].SetSource (soundObject.AddComponent<AudioSource> ());
				soundObject.transform.SetParent (this.transform);
				soundObject.transform.localPosition = Vector3.zero;
			}
		}
	}

	public void ClipsToSounds ()
	{
		int clipsLength = clips.Length;
		sounds = new Sound[clipsLength];
		int i = 0;
		foreach (AudioClip clip in clips) {
			sounds [i] = new Sound ();
			sounds [i].name = clip.name;
			sounds [i].clip = clip;
			sounds [i].loop = false;
			sounds [i].randomPitch = 0.0f;
			sounds [i].randomVolume = 0.0f;
			sounds [i].masterPitch = 1.0f;
			sounds [i].masterVolume = 1.0f;
			sounds [i].pitch = 1.0f;
			sounds [i].volume = 1.0f;
			i++;
		}
		clips = new AudioClip[0];
	}
	
	public void AddClipsToSounds ()
	{
		int clipsLength = clips.Length;
		int soundsLength = sounds.Length;
		var tempSounds = sounds;
		sounds = new Sound[soundsLength+clipsLength];
		
		int i = 0;

		foreach (var sound in tempSounds)
		{
			sounds[i] = tempSounds[i];
			i++;
		}
		foreach (AudioClip clip in clips) {
			sounds [i] = new Sound ();
			sounds [i].name = clip.name;
			sounds [i].clip = clip;
			sounds [i].loop = false;
			sounds [i].randomPitch = 0.0f;
			sounds [i].randomVolume = 0.0f;
			sounds [i].masterPitch = 1.0f;
			sounds [i].masterVolume = 1.0f;
			sounds [i].pitch = 1.0f;
			sounds [i].volume = 1.0f;
			i++;
		}
		clips = new AudioClip[0];
	}

	public void DropEmptyNamed ()
	{
		int soundsToStayCount = 0;
		int j = 0;
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name != "") {
				soundsToStayCount++;
			}
		}

		Sound[] soundsToStay = new Sound[soundsToStayCount];

		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name != "") {
				soundsToStay [j] = sounds [i];
				j++;
			}
		}

		sounds = new Sound[soundsToStayCount];

		for (int i = 0; i < sounds.Length; i++) {
			sounds [i] = soundsToStay [i];
		}
	}
	

	public void PlaySound (string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				sounds [i].Play ();
				return;
			}
		}
		Debug.LogWarning ("Sound with name " + soundName + " not found!");
	}

	public void StopSound (string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				sounds [i].Stop ();
				return;
			}
		}
		Debug.LogWarning ("Sound with name " + soundName + " not found!");
	}

	public void StopAllSounds ()
	{
		for (int i = 0; i < sounds.Length; i++) {
			sounds [i].Stop ();
		}
	}

	public void SetVolume (float volume)
	{
		for (int i = 0; i < sounds.Length; i++) {
			sounds [i].masterVolume = volume;
		}
	}

	public void SetPitch (float pitch)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name != "button") {
				sounds [i].masterPitch = pitch;
			}
		}
	}

	public void MuteSound (string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				sounds [i].Mute ();
				return;
			}
		}
	}

	public void PauseSound(string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				sounds [i].Pause();
				return;
			}
		}
	}

	public void UnmuteSound (string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				sounds [i].Unmute ();
				return;
			}
		}
	}

	public float GetSoundLenght(string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				return  sounds[i].GetSoundLenght();
			}
		}
		return 0;
	}
	public float GetSoundCurPosition(string soundName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (sounds [i].name == soundName) {
				return sounds[i].GetSoundCurPosition();
			}
		}
		return 0;
	}
}

#if UNITY_EDITOR
[CustomEditor(typeof(SoundManager))]
public class SoundManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		SoundManager script = (SoundManager)target;
		if (GUILayout.Button ("Add clips to sounds")) {
			script.AddClipsToSounds ();
		}
		if (GUILayout.Button ("Convert clips to sounds")) {
			script.ClipsToSounds ();
		}

		if (GUILayout.Button ("Drop empty named sounds")) {
			script.DropEmptyNamed ();
		}
		
	}
}
#endif