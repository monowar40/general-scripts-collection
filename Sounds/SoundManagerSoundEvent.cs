﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Дополнение к SoundManager'у, позволяет отлавливать события начала-конца звука, при запуске отсюда.
///
/// TODO: переписать весь этот код на делегатах из soundManager'a
/// </summary>
[Serializable]
public class SoundEvent
{
	public enum SoundEventType
	{
		OnStartSound,
		OnEndSound,
		OnDelay
	}
	public SoundEventType _soundEventType;
	[Tooltip("Работает в OnDelay для обозначения задержки перед вызовом события и в OnEndSound - для вызова события раньше конца озвучки")]
	public float InvokeTime;
	public UnityEvent SMSoundEvent;
}
[Serializable]
public class EventedSound
{
	public string soundName;
	public List<SoundEvent> SoundEvents = new List<SoundEvent>();
	private SoundManager _soundManager;
	public void Init(SoundManager soundManagerComponent)
	{
		_soundManager = soundManagerComponent;
	}
	public void Play()
	{
		_soundManager.PlaySound(soundName);

		StopAllCurEvents();
		AddEvents();
	}

	private void StopAllCurEvents()
	{
		_soundManager.StopAllCoroutines();
	}
	private void AddEvents()
	{
		SoundEvents.ForEach(ae =>
		{
			switch (ae._soundEventType)
			{ 
				case SoundEvent.SoundEventType.OnDelay: 
					_soundManager.StartCoroutine(InvokeAction.Timer(ae.InvokeTime, 
						() => { if (ae.SMSoundEvent != null) ae.SMSoundEvent.Invoke(); })); 
					break;
				
				case SoundEvent.SoundEventType.OnEndSound:
					_soundManager.StartCoroutine(InvokeAction.Timer(_soundManager.GetSoundLenght(soundName) - ae.InvokeTime, () =>
					{
						if (ae.SMSoundEvent != null) ae.SMSoundEvent.Invoke();
					}));
					break;
				
				case SoundEvent.SoundEventType.OnStartSound:
					if (ae.SMSoundEvent != null) 
						ae.SMSoundEvent.Invoke();
					break; 
			}
		});
	}
	
	public void Stop()
	{
		_soundManager.StopSound(soundName);
		StopAllCurEvents();
	}
}
public class SoundManagerSoundEvent : MonoBehaviour
{
	public SoundManager soundManager;
	public List<EventedSound> Sounds = new List<EventedSound>();


	private void Start()
	{
		if(soundManager == null)
			soundManager = GetComponent<SoundManager>();
		InitSounds(soundManager);
	}
	
	public void PlaySound(string soundName)
	{
		var tmp = Sounds.First(sound => sound.soundName == soundName);
		tmp.Play();
	}

	public void StopAllSound()
	{
		soundManager.StopAllSounds();
		StopAllCoroutines();
	}

	public void StopSound(string soundName)
	{
		var tmp = Sounds.First(sound => sound.soundName == soundName);
		tmp.Stop();
	}

	private void InitSounds(SoundManager soundManagerComponent)
	{
		Sounds.ForEach(sound => sound.Init(soundManagerComponent));
	}

	private void OnDestroy()
	{
		StopAllCoroutines();
	}
	
}
