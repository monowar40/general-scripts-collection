using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Простейший пулл объектов, отдает-возвращает GameObject'ы, может переполняться
///
/// Пока что недоделан, есть слабые места, WIP, так сказать
/// </summary>
[Serializable]
public class InitialPooledObject
{
    public PooledObject pooledObject;
    public int initialCount;
}
[Serializable]
public class PooledObject
{
    public string objectName;
    public GameObject pooledGameObject;
    [HideInInspector] public bool available;
}
public class ObjectPool : MonoBehaviour
{
    [SerializeField] private List<InitialPooledObject> initialPooledObjects = new List<InitialPooledObject>();

    [ReadOnly][SerializeField] private List<PooledObject> objectsInPool = new List<PooledObject>();

    private void Awake()
    {
        InitializePool();
    }

    public GameObject GetObjectFromPool(string objectName)
    {
        //todo remove po.objectName.Contains
        var objInPool= objectsInPool.Find(po => po.objectName.Contains(objectName)&& po.available);
        
        if (objInPool == null)
        {
            objInPool = GetPooledObjectByName(objectName);
        }
        objInPool.available = false;
        objInPool.pooledGameObject.SetActive(true);
        return objInPool.pooledGameObject;
    }

    public void ReturnObjectToPool(GameObject go)
    {
        go.SetActive(false);
        go.transform.SetParent(transform);
        var objInPool = objectsInPool.Find(po => po.pooledGameObject == go);
        objInPool.available = true;
    }

    private void InitializePool()
    {
        for (int i = 0; i < initialPooledObjects.Count; i++)
        {
            for (int j = 0; j < initialPooledObjects[i].initialCount; j++)
            {
                PooledObject pooledObject = initialPooledObjects[i].pooledObject;
                InstantiateNewObject(pooledObject);

            }
        }
    }
    private PooledObject GetPooledObjectByName(string objectName)
    {
        var tempObjectFromInitialList = InstantiateNewObject(initialPooledObjects.Find(o =>
            o.pooledObject.objectName.Contains(objectName)).pooledObject);
        if (tempObjectFromInitialList != null)
        {
            return tempObjectFromInitialList;
        }
        else
        {
            Debug.LogError($"Object with name {objectName} not found");
            return null;
        }
    }
    private PooledObject InstantiateNewObject(PooledObject pooledObject)
    {
        var obj = Instantiate(pooledObject.pooledGameObject, Vector3.zero, Quaternion.identity, transform);
        obj.name = pooledObject.objectName;
        obj.SetActive(false);

        PooledObject objInPool = new PooledObject
        {
            pooledGameObject = obj,
            available = true,
            objectName = obj.name+"_"+objectsInPool.Count
        };
        objectsInPool.Add(objInPool);
        return objInPool;
    }
}
