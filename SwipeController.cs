﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Простой обработчик свайпов
/// </summary>

public class SwipeController : MonoBehaviour
{
    [SerializeField] private float minDeltaToSwipe = 0.5f;
    
    [SerializeField] private UnityEvent onLeftSwipe;
    [SerializeField] private UnityEvent onRightSwipe;
    [SerializeField] private UnityEvent onUpSwipe;
    [SerializeField] private UnityEvent onDownSwipe;


    private RuntimePlatform _platform => Application.platform;
    
    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    private void Awake()
    {
        firstPressPos = Vector2.zero;
        secondPressPos = Vector2.zero;
        currentSwipe = Vector2.zero;

    }
    private void Swipe()
    {
        if(Input.touches.Length > 0)
        {
            Debug.Log(Input.touches[0].position);

            Touch t = Input.GetTouch(0);
            if(t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x,t.position.y);
            }
            if(t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x,t.position.y);
                       
                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
           
                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe upwards
                if(currentSwipe.y > 0 &&  currentSwipe.x > -minDeltaToSwipe && currentSwipe.x < minDeltaToSwipe)
                {
                    onUpSwipe?.Invoke();
                    Debug.Log("up swipe");
                }
                //swipe down
                if(currentSwipe.y < 0 && currentSwipe.x > -minDeltaToSwipe && currentSwipe.x < minDeltaToSwipe)
                {
                    onDownSwipe?.Invoke();
                    Debug.Log("down swipe");
                }
                //swipe left
                if(currentSwipe.x < 0 && currentSwipe.y > -minDeltaToSwipe && currentSwipe.y < minDeltaToSwipe)
                {
                    onLeftSwipe?.Invoke();
                    Debug.Log("left swipe");
                }
                //swipe right
                if(currentSwipe.x > 0 && currentSwipe.y > -minDeltaToSwipe && currentSwipe.y < minDeltaToSwipe)
                {
                    onRightSwipe?.Invoke();
                    Debug.Log("right swipe");
                }
            }
        }
    }
    
    
    
    private void MouseSwipe()
    {
        if(Input.GetMouseButtonDown(0))
        {
            firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
        }
        if(Input.GetMouseButtonUp(0))
        {
            secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
            
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            currentSwipe.Normalize();

            //swipe upwards
            if(currentSwipe.y > 0 && currentSwipe.x > -minDeltaToSwipe && currentSwipe.x < minDeltaToSwipe)
            {
                onUpSwipe?.Invoke();
                Debug.Log("up swipe");
            }
            //swipe down
            if(currentSwipe.y < 0 && currentSwipe.x > -minDeltaToSwipe && currentSwipe.x < minDeltaToSwipe)
            {
                onDownSwipe?.Invoke();
                Debug.Log("down swipe");
            }
            //swipe left
            if(currentSwipe.x < 0 && currentSwipe.y > -minDeltaToSwipe && currentSwipe.y < minDeltaToSwipe)
            {
                onLeftSwipe?.Invoke();
                Debug.Log("left swipe");
            }
            //swipe right
            if(currentSwipe.x > 0 && currentSwipe.y > -minDeltaToSwipe && currentSwipe.y < minDeltaToSwipe)
            {
                onRightSwipe?.Invoke();
                Debug.Log("right swipe");
            }
        }
    }

    private void Update()
    {
        if (_platform == RuntimePlatform.Android || _platform == RuntimePlatform.IPhonePlayer)
            Swipe();
        else 
            MouseSwipe();
    }
}



