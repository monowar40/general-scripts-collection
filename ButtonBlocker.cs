﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Блокирует и разблокирует кнопки в листе
/// </summary>
 public class ButtonBlocker : MonoBehaviour
 {

     [SerializeField] private List<Button> buttonsToBlock;
     private bool _buttonIsBlock;

     public void BlockButtons()
     {
         buttonsToBlock.ForEach(button => { button.interactable = false; });
         _buttonIsBlock = true;
     }

     public void UnBlockButtons()
     {
         buttonsToBlock.ForEach(button => { button.interactable = true; });
         _buttonIsBlock = false;
     }

     public void SwitchButtonBlocks()
     {
         if (_buttonIsBlock)
             UnBlockButtons();
         else
             BlockButtons();

     }
 }

