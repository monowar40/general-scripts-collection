﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Считалка FPS. Отображает в текстовых полях текущий, максимальный, минимальный, средний FPS.
/// Так же позволяет задать Application.targetFrameRate
/// </summary>
public class FPSCounter : MonoBehaviour
{
    [SerializeField] private Text fpsDisplay;
    [SerializeField] private Text averageFPSDisplay;
    [SerializeField] private Text minFPSDisplay, maxFPSDisplay;
    [SerializeField] private int setTargetFrameRate = 0;
    
    private int _framesPassed;
    private float _fpsTotal;
    private float _minFPS = Mathf.Infinity;
    private float _maxFPS;

    void Start() {
        if(setTargetFrameRate > 0)
            Application.targetFrameRate = setTargetFrameRate;
    }

    void Update()
    {
        float fps = 1 / Time.unscaledDeltaTime;
        fpsDisplay.text = "" + (float)Math.Round(fps);

        _fpsTotal += fps;
        _framesPassed++;
        averageFPSDisplay.text = "" + (float)Math.Round((_fpsTotal / _framesPassed));

        if (fps > _maxFPS && _framesPassed > 10) {
            _maxFPS = fps;
            maxFPSDisplay.text = "" + (float)Math.Round(_maxFPS);
        }
        if (fps < _minFPS && _framesPassed > 10) {
            _minFPS = fps;
            minFPSDisplay.text = "" + (float)Math.Round(_minFPS);
        }
    }

}
