﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// получает текст у TextManager'а и вставляет его в текстовые поля
/// </summary>
public class TextSetter : MonoBehaviour
{
    [SerializeField] private string index;
    [SerializeField] private TextMeshProUGUI textTMPro;
    [SerializeField] private Text text;


    void Start()
    {
        SetText();
    }

    public void SetText()
    {
        if(textTMPro)
            textTMPro.text = TextManager.Instance.GetText(index);
        if(text)
            text.text = TextManager.Instance.GetText(index);
    }
}
