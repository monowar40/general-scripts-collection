﻿using UnityEngine;

/// <summary>
/// Наипростейший отдаватель текста из json'а
/// </summary>

public class TextManager : ASingleton_MBLazy<TextManager>
{
	[SerializeField] TextAsset textAsset;
	private JSON.ANode textsNode;


	void Start ()
	{
        Parse();
    }

    void Parse()
    {
        textsNode = JSON.Parse(textAsset.text);
    }

    public string GetText (string textIndex)
	{
		return textsNode [textIndex].Value.ToString ();
	}

    public void SetTextAsset (TextAsset newTextAsset)
    {
        textAsset = newTextAsset;
        Parse();
    }
}
