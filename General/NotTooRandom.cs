﻿using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Это - хитрый псевдорандом, избегающий повторений. Фактически, генерируется полная "колода" всех вариантов, после чего он её хорошо тасует и начинает отдавать карты по одной.
/// Это гарантирует отсутствие повторения двух значений подряд.
/// </summary>
public class NotTooRandom
{
    private readonly List<int> _values;

    /// <summary>
    /// Inclusive
    /// </summary>
    public readonly int Min;

    /// <summary>
    /// Exclusive
    /// </summary>
    public readonly int Max;

    public bool AvoidRepeats = true;

    public int PreviousValue { get; private set; }
    
    /// <summary>
    /// Returns "not too random" numbers in between 0 [inclusive] and max [exclusive].
    /// </summary>
    /// <param name="max">Exclusive</param>
    /// <param name="avoidRepetition"></param>
    public NotTooRandom(int max, bool avoidRepetition = true)
        : this(0, max, avoidRepetition)
    {
    }

    /// <summary>
    /// Returns "not too random" numbers in between min [inclusive] and max [exclusive].
    /// </summary>
    /// <param name="min">Inclusive</param>
    /// <param name="max">Exclusive</param>
    ///  /// <param name="avoidRepetition"></param>
    public NotTooRandom(int min, int max, bool avoidRepetition = true)
    {
        AvoidRepeats = avoidRepetition;

        if (max < min)
        {
            Debug.LogWarning("NotTooRandom: Max value should be greater than min!"); //TODO1: Print min, max
            Min = max;
            Max = min;
        }
        else
        {
            if (max == min)
            {
                Debug.LogError("NotTooRandom: Max value should be greater than min!"); //TODO1: Print min, max
                Max = min + 1;
                Min = min;
            }
            else
            {
                Min = min;
                Max = max;
            }
        }

        if (min + 1 == max)
        {
            Debug.LogWarning("NotTooRandom: Max and min values have difference of ONE! We'll make it as random as we can, of course."); //TODO1: Print min, max
        }

        _values = new List<int>();
    }

    private static void Swap(IList<int> arg, int i1, int i2)
    {
        int tmp = arg[i1];
        arg[i1] = arg[i2];
        arg[i2] = tmp;
    }

    public void Reset()
    {
        int c = Max - Min; // 2 = 3 - 1 (1, 2)
        int[] res = new int[c];

        for (int i = Min; i < Max; ++i)
        {
            res[i - Min] = i;
        }

        int to = c + Random.Range(0, c);
        for (int i = 0; i < to; ++i)
        {
            Swap(res, Random.Range(0, c - 1), Random.Range(0, c - 1));
        }

        if (AvoidRepeats && res[0] == PreviousValue && c > 1)
        {
            Swap(res, 0, Random.Range(1, c - 1));
        }

        _values.Clear();
        _values.AddRange(res);
    }

    public bool IsSameBoundsAs(NotTooRandom other)
    {
        return Max == other.Max && Min == other.Min;
    }

    public int Next()
    {
        if (_values.Count == 0)
            Reset();

        int res = _values[0];
        _values.RemoveAt(0);

        PreviousValue = res;

        return res;
    }
}


