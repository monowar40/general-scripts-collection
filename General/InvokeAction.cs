﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Обертки над IEnumerator'ами, позволяют использовать их как то так:
/// StartCoroutine(InvokeAction.Timer(1, () => { Debug.Log("Прошла 1 сек"); }));
/// </summary>

public static class InvokeAction
{
    
    public static IEnumerator FixedUpdate(Action action)
    {
        yield return new WaitForFixedUpdate();

        if (action != null)
        {
            action.Invoke();
        }
    }
    
    public static IEnumerator Timer(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);

        if (action != null)
        {
            action.Invoke();
        }
    }
            
    public static IEnumerator WaitUntil(Func<bool> predicate, Action action)
    {
        yield return new WaitUntil(predicate);

        if (action != null)
        {
            action.Invoke();
        }
    }
    public static IEnumerator EndOfFrame(Action action)
    {
        yield return new WaitForEndOfFrame();

        if (action != null)
        {
            action.Invoke();
        }
    }
}
