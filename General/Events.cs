using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Обертка некоторых стандартных типов в UnityEvent
/// </summary>
[System.Serializable]
public class IntEvent : UnityEvent<int>
{}
[System.Serializable]
public class FloatEvent : UnityEvent<float>
{}
[System.Serializable]
public class BoolEvent : UnityEvent<bool>
{}
[System.Serializable]
public class Vec2Event : UnityEvent<Vector2>
{}
[System.Serializable]
public class Vec3Event : UnityEvent<Vector3>
{}
[System.Serializable]
public class TransformEvent : UnityEvent<Transform>
{}
[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject>
{}