﻿//This s a refactored and improved version of SimpleJSON

/* * * * *
 * A simple JSON Parser / builder
 * ------------------------------
 * 
 * It mainly has been written as a simple JSON parser. It can build a JSON string
 * from the node-tree, or generate a node tree from any valid JSON string.
 * 
 * If you want to use compression when saving to file / stream / B64 you have to include
 * SharpZipLib ( http://www.icsharpcode.net/opensource/sharpziplib/ ) in your project and
 * define "USE_SharpZipLib" at the top of the file
 * 
 * Written by Bunny83 
 * 2012-06-09
 * 
 * Modified by oPless, 2014-09-21 to round-trip properly
 *
 * Features / attributes:
 * - provides strongly typed node classes and lists / dictionaries
 * - provides easy access to class members / array items / data values
 * - the parser ignores data types. Each value is a string.
 * - only double quotes (") are used for quoting strings.
 * - values and names are not restricted to quoted strings. They simply add up and are trimmed.
 * - There are only 3 types: arrays(JSONArray), objects(JSONClass) and values(JSONData)
 * - provides "casting" properties to easily convert to / from those types:
 *   int / float / double / bool
 * - provides a common interface for each node so no explicit casting is required.
 * - the parser try to avoid errors, but if malformed JSON is parsed the result is undefined
 * 
 * 
 * 2012-12-17 Update:
 * - Added internal JSONLazyCreator class which simplifies the construction of a JSON tree
 *   Now you can simple reference any item that doesn't exist yet and it will return a JSONLazyCreator
 *   The class determines the required type by it's further use, creates the type and removes itself.
 * - Added binary serialization / deserialization.
 * - Added support for BZip2 zipped binary format. Requires the SharpZipLib ( http://www.icsharpcode.net/opensource/sharpziplib/ )
 *   The usage of the SharpZipLib library can be disabled by removing or commenting out the USE_SharpZipLib define at the top
 * - The serializer uses different types when it comes to store the values. Since my data values
 *   are all of type string, the serializer will "try" which format fits best. The order is: int, float, double, bool, string.
 *   It's not the most efficient way but for a moderate amount of data it should work on all platforms.
 * 
 * * * * */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;



    /// <summary>
    /// Это SimpleJSON, который без труда гуглится. Но сильно подрихтованный и обмазанный синтаксическим сахаром.
    /// </summary>
    public static class JSON
    {
        public interface IWriteable
        {
            /// <summary>
            /// Сериализация в JSON
            /// </summary>
            /// <param name="parent">Родительская нода, запись будет произведена в неё</param>
            /// <param name="forceName">Опциональный аргумент, не все объекты восприимчивы к нему при сериализации. Позволяет снаружи задать имя генерируемой ноды.</param>
            void JsonWriteTo(ANode parent, string forceName = "");
        }

        public interface ISerializable
        {
            /// <summary>
            /// Сериализация в JSON
            /// </summary>
            /// <param name="parent">Родительская нода, запись будет произведена в неё</param>
            /// <param name="forceName">Опциональный аргумент, не все объекты восприимчивы к нему при сериализации. Позволяет снаружи задать имя генерируемой ноды.</param>
            void JsonWriteTo(ANode parent, string forceName = "");

            /// <summary>
            /// Десериализация из JSON
            /// </summary>
            /// <param name="arg">Нода, содержащая данные, которые будут десериализованы</param>
            /// <param name="outerName">Опциональный аргумент, не все объекты восприимчивы к нему. 
            /// В случае если JSON представление не хранит имя объекта, при десериализации оно берется отсюда.</param>
            void JsonReadFrom(ANode node, string outerName = "");
        }

        public enum BinaryTag
        {
            Array = 1,
            Class = 2,
            Value = 3,
            IntValue = 4,
            DoubleValue = 5,
            BoolValue = 6,
            FloatValue = 7,
        }

        public static ANode Parse(string aJSON)
        {   
            return ANode.Parse(aJSON);
        }

        #region Reading and writing unity types

        public static bool TryRead(ANode node, out JSON.ANode variable, params string[] names)
        {
            variable = null;
            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                variable = n;
                return true;
            }
            return false;
        }

        public static bool TryReadStrings(ANode node, ref string [] variable, params string[] names)
        {
            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (n is Array)
                {
                    variable = n.AsArray.Children.Select(x => x.Value).ToArray();
                }
                else if (n is Data)
                {
                    variable = new[] { n.Value };
                }
                else
                {
                    Debug.LogError("JSON.TryReadStrings: Error at \"" + s + "\": Array or Data expected!");
                    continue;
                }
                return true;
            }
            return false;
        }

        public static void ReadStrings(ANode node, ref string[] variable)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadStrings: Null node received!");
            }
            else if (node is Array)
            {
                variable = node.AsArray.Children.Select(x => x.Value).ToArray();
            }
            else if (node is Data)
            {
                variable = new[] { node.Value };
            }
            else
            {
                Debug.LogError("JSON.ReadStrings: Error at \"" + node.ToJSON() + "\": Array or Data expected!");
            }
        }

        public static void ReadInt(ANode node, ref int variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadInt: Error at \"" + node.ToJSON() + "\": Null!");
                return;
            }
            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (n is Data)
                {
                    variable = n.AsInt;
                    return;
                }
                Debug.LogError("JSON.ReadInt: Error at \"" + s + "\": Data expected!");
            }
            Debug.LogError("JSON.ReadInt: Error! not found in " + node.ToJSON());
        }

        public static void ReadString(ANode node, ref string variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadString: Error at \"" + node.ToJSON() + "\": Null!");
                return;
            }
            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (n is Data)
                {
                    variable = n.Value;
                    return;
                }
                Debug.LogError("JSON.ReadString: Error at \"" + s + "\": Data expected!");
            }
            Debug.LogError("JSON.ReadString: Error!  not found in " + node.ToJSON());
        }

        public static void ReadInts(ANode node, ref int[] variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadInts: Error at \"" + node.ToJSON() + "\": Null!");
                return;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (ReadInts(n, ref variable))
                {
                    return;
                }
            }
            Debug.LogError("JSON.ReadInts: Error! Names not found in " + node.ToJSON());
        }

        public static bool ReadInts(ANode node, ref int[] variable)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadInts: Null node received!");
            }
            else if (node is Array)
            {
                variable = node.AsArray.Children.Select(x => x.AsInt).ToArray();
                return true;
            }
            else if (node is Data)
            {
                variable = new[] { node.AsInt };
                return true;
            }
            else
            {
                Debug.LogError("JSON.ReadInts: Error at \"" + node.ToJSON() + "\": Array or Data expected!");
            }
            return false;
        }

        public static void ReadDateTime(ANode node, ref DateTime variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadDateTime: Null node received!");
                return;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (ReadDateTime(n, ref variable))
                {
                    return;
                }
            }
            Debug.LogError("JSON.ReadDateTime: Error! Names not found in " + node.ToJSON());
        }

        public static bool ReadDateTime(ANode node, ref DateTime variable)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadDateTime: Null node received!");
            }
            else if (node is Data)
            {
                if (DateTime.TryParse(node.Value, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out var dt))
                {
                    variable = dt;
                    return true;
                }
                else
                {
                    Debug.LogError("JSON.ReadDateTime: Failed to parse DateTime from string: " + node.Value);
                }
            }
            else
            {
                Debug.LogError("JSON.ReadDateTime: Error at \"" + node.ToJSON() + "\": Data expected!");
            }
            return false;
        }

        public static void ReadBool(ANode node, ref bool variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadBool: Error at \"" + node.ToJSON() + "\": Null!");
                return;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (n is Data)
                {
                    variable = n.AsBool;
                    return;
                }
                Debug.LogError("JSON.ReadBool: Error at \"" + s + "\": Data expected!");
            }
            Debug.LogError("JSON.ReadBool: Error! Names not found in " + node.ToJSON());
        }

        public static bool TryReadInts(ANode node, ref int [] variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.ReadInts: Null node received!");
                return false;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                if (n is Array)
                {
                    variable = n.AsArray.Children.Select(x => x.AsInt).ToArray();
                }
                else if (n is Data)
                {
                    variable = new[] { n.AsInt };
                }
                else
                {
                    Debug.LogError("JSON.TryReadInts: Error at \"" + s + "\": Array or Data expected!");
                    continue;
                }
                return true;
            }
            return false;
        }

        public static bool TryReadFloat(ANode node, ref float variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.TryReadFloat: Null node received!");
                return false;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                variable = n.AsFloat;
                return true;
            }
            return false;
        }

        public static bool TryReadInt(ANode node, ref int variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.TryReadInt: Null node received!");
                return false;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }

                variable = n.AsInt;
                return true;
            }

            return false;
        }

        public static bool TryReadBool(ANode node, ref bool variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.TryReadBool: Null node received!");
                return false;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }

                variable = n.AsBool;
                return true;
            }

            return false;
        }

        public static bool TryReadString(ANode node, ref string variable, params string[] names)
        {
            if (node == null)
            {
                Debug.LogError("JSON.TryReadString: Null node received!");
                return false;
            }

            foreach (string s in names)
            {
                ANode n = node[s];
                if (n == null)
                {
                    continue;
                }
                variable = n.Value;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Получение Quaternion из json
        /// </summary>
        /// <param name="node">Узел JSON</param>
        /// <returns> Quaternion </returns>
        public static Quaternion ReadQuaternion(JSON.ANode node)
        {
            float x = -1, y = -1, z = -1, w = -1;
            if (TryReadFloat(node, ref x, "x") && TryReadFloat(node, ref y, "y") && TryReadFloat(node, ref z, "z"))
            {
                return TryReadFloat(node, ref w, "w") ? new Quaternion(x, y, z, w) : Quaternion.Euler(x, y, z);
            }
            Debug.LogError("JSON.ReadQuaternion: Bad Quaternion: " + node.ToJSON());
            return Quaternion.identity;
        }

        /// <summary>
        /// Получение Vector3 из json
        /// </summary>
        /// <param name="node">Узел JSON</param>
        /// <returns> Vector3 </returns>
        public static Vector3 ReadVector3(JSON.ANode node)
        {
            float x = -1, y = -1, z = -1;
            if (TryReadFloat(node, ref x, "x") && TryReadFloat(node, ref y, "y") && TryReadFloat(node, ref z, "z"))
            {
                return new Vector3(x, y, z);
            }
            Debug.LogError("JSON.ReadVector3: Bad Vector3: " + node.ToJSON());
            return Vector3.zero;
        }

        /// <summary>
        /// Получение Vector2 из json
        /// </summary>
        /// <param name="node">Узел JSON</param>
        /// <returns> Vector2 </returns>
        public static Vector2 ReadVector2(JSON.ANode node)
        {
            return new Vector2(node["x"].AsFloat, node["y"].AsFloat);
        }

        public static AnimationEvent ReadAnimationEvent(JSON.ANode node)
        {
            AnimationEvent e = new AnimationEvent
            {
                time = node["time"].AsFloat,
                functionName = node.GetOneOf("functionName").Value
            };

            ANode sPar = node.GetOneOf("stringParameter", "stringParam");
            if (sPar != null)
            {
                e.stringParameter = sPar.Value;
            }

            ANode fPar = node.GetOneOf("floatParameter", "floatParam");
            if (fPar != null)
            {
                e.floatParameter = fPar.AsFloat;
            }

            ANode iPar = node.GetOneOf("intParameter", "intParam");
            if (iPar != null)
            {
                e.intParameter = iPar.AsInt;
            }
        
            return e;
        }

        public static bool TryReadClassesChildren(ANode suposedClass, Dictionary<string, ANode>  destination, IEnumerable<string> exceptions = null)
        {   
            Class nodeC = suposedClass as Class;
            if (nodeC == null)
            {
                Debug.LogError("JSON.GetClassesChildrenExcept: Argument is not a class! Arg: " + (suposedClass?.ToJSON() ?? "null"));
                return false;
            }
            bool res = false;
            for (int i = 0; i < nodeC.Count; ++i)
            {
                string name = nodeC.GetChildNameAt(i);
                if (exceptions == null || !exceptions.Contains(name))
                {
                    res = true;
                    if (destination.ContainsKey(name))
                    {
                        destination[name] = nodeC.GetChildAt(i);
                    }
                    else
                    {
                        destination.Add(name, nodeC.GetChildAt(i));
                    }
                }
            }
            return res;
        }

        #endregion

        public abstract class ANode
        {
            #region common interface

            public virtual ANode Add(string aKey, ANode aItem, bool allowNulls = true)
            {
                return this;
            }

            public virtual ANode Add(string aKey, int value)
            {
                return Add(aKey, new Data(value));
            }

            public virtual ANode Add(string aKey, string value)
            {
                return Add(aKey, new Data(value));
            }

            public virtual ANode Add(string aKey, float value)
            {
                return Add(aKey, new Data(value));
            }

            public virtual ANode this[int aIndex] { get { return null; } set { } }

            public virtual ANode this[string aKey] { get { return null; } set { } }

            public virtual string Value { get { return ""; } set { } }

            public virtual int Count => 0;

            public virtual ANode Add(ANode aItem)
            {
                return Add("", aItem);
            }

            public virtual ANode Remove(string aKey)
            {
                return null;
            }

            public virtual ANode Remove(int aIndex)
            {
                return null;
            }

            public virtual ANode Remove(ANode aNode)
            {
                return aNode;
            }

            public ANode GetOneOf(params string[] keys)
            {
                return keys.Select(s => this[s]).FirstOrDefault(n => n != null);
            }

            public virtual IEnumerable<ANode> Children
            {
                get
                {
                    yield break;
                }
            }

            public IEnumerable<ANode> DeepChildren
            {
                get { return Children.SelectMany(C => C.DeepChildren); }
            }

            public override string ToString()
            {
                return "JSONNode";
            }

            public virtual string ToString(string aPrefix)
            {
                return "JSONNode";
            }

            public abstract string ToJSON(int prefix = 0);

            #endregion common interface

            #region typecasting properties

            public virtual BinaryTag Tag { get; set; }

            public virtual int AsInt
            {
                get => int.TryParse(Value, out int v) ? v : 0;
                set
                {
                    Value = value.ToString();
                    Tag = BinaryTag.IntValue;
                }
            }

            public virtual float AsFloat
            {
                get => float.TryParse(Value, out float v) ? v : 0.0f;
                set
                {
                    Value = value.ToString().Replace(',', '.');
                    Tag = BinaryTag.FloatValue;
                }
            }

            public virtual double AsDouble
            {
                get => double.TryParse(Value, out double v) ? v : 0;
                set
                {
                    Value = value.ToString();
                    Tag = BinaryTag.DoubleValue;
                }
            }

            public virtual bool AsBool
            {
                get
                {
                    if (bool.TryParse(Value, out bool b))
                    {
                        return b;
                    }
                    if (int.TryParse(Value, out int i))
                    {
                        return i != 0;
                    }
                    return !string.IsNullOrEmpty(Value);
                }
                set
                {
                    Value = value ? "true" : "false";
                    Tag = BinaryTag.BoolValue;
                }
            }

            public virtual Array AsArray => this as Array;

            [Obsolete("Use AsClass instead")]
            public virtual Class AsObject => this as Class;

            public virtual Class AsClass => this as Class;

            #endregion typecasting properties

            #region operators
            
            public static implicit operator string(ANode d)
            {
                return d == null ? "null" : d.Value;
            }

            public static bool operator ==(ANode a, object b)
            {
                if (b == null)
                {   
                    if (a is FakeNode)
                    {
                        return true;
                    }
                }
                return ReferenceEquals(a, b);
            }

            public static bool operator !=(ANode a, object b)
            {
                return !(a == b);
            }

            public override bool Equals(object obj)
            {
                return ReferenceEquals(this, obj);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            #endregion operators

            internal static string Escape(string aText)
            {
                string result = "";
                foreach (char c in aText)
                {
                    switch (c)
                    {
                        case '\\':
                            result += "\\\\";
                            break;
                        case '\"':
                            result += "\\\"";
                            break;
                        case '\n':
                            result += "\\n";
                            break;
                        case '\r':
                            result += "\\r";
                            break;
                        case '\t':
                            result += "\\t";
                            break;
                        case '\b':
                            result += "\\b";
                            break;
                        case '\f':
                            result += "\\f";
                            break;
                        default:
                            result += c;
                            break;
                    }
                }
                return result;
            }

            private static Data Numberize(string token, string tokenName = "")
            {
                if (int.TryParse(token, out var integer))
                {
                    return new Data(integer);
                }

                if (double.TryParse(token, NumberStyles.Any, CultureInfo.InvariantCulture, out var real))
                {
                    return new Data(real);
                }

                if (bool.TryParse(token, out var flag))
                {
                    return new Data(flag);
                }

                if (token == "null")
                {
                    return null;
                }

                Debug.LogError("JSON: Failed to read JSON Data node. Token: \""+token+ "\", Token name: \"" + tokenName + "\"");
                return new Data(-1);
            }

            private static void AddElement(ANode ctx, string token, string tokenName, bool tokenIsString)
            {
                if (tokenIsString)
                {
                    if (token == "@#$%EMPTYSTRING")
                        token = "";
                    if (ctx is Array)
                    {
                        ctx.Add(new Data(token));
                    }
                    else
                    {
                        ctx.Add(tokenName, new Data(token)); // assume dictionary/object
                    }
                }
                else
                {
                    Data number = Numberize(token, tokenName);
                    if (ctx is Array)
                    {
                        ctx.Add(number);
                    }
                    else
                    {
                        ctx.Add(tokenName, number);
                    }
                }
            }
         
            public static ANode Parse(string aJSON)
            {
                Stack<ANode> stack = new Stack<ANode>();
                ANode ctx = null;
                int i = 0;
                var Token = new StringBuilder();
                var TokenName = new StringBuilder();
                bool QuoteMode = false;
                bool TokenIsString = false;
                while (i < aJSON.Length)
                {
                    switch (aJSON[i])
                    {
                        case '{':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                                break;
                            }
                            stack.Push(new Class());
                            if (ctx != null)
                            {
                                //TokenName = TokenName.Trim();
                                if (ctx is Array)
                                    ctx.Add(stack.Peek());
                                else if (TokenName.Length != 0)
                                    ctx.Add(TokenName.ToString(), stack.Peek());
                            }
                            TokenName = new StringBuilder();
                            Token = new StringBuilder();
                            ctx = stack.Peek();
                            break;

                        case '[':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                                break;
                            }

                            stack.Push(new Array());
                            if (ctx != null)
                            {
                                //TokenName = TokenName.Trim();

                                if (ctx is Array)
                                    ctx.Add(stack.Peek());
                                else if (TokenName.Length != 0)
                                    ctx.Add(TokenName.ToString(), stack.Peek());
                            }
                            TokenName = new StringBuilder();
                            Token = new StringBuilder();
                            ctx = stack.Peek();
                            break;

                        case '}':
                        case ']':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                                break;
                            }
                            if (stack.Count == 0)
                                throw new Exception("JSON Parse: Too many closing brackets");

                            stack.Pop();
                            if (Token.Length != 0)
                            {
                                //TokenName = TokenName.Trim();
                                AddElement(ctx, Token.ToString(), TokenName.ToString(), TokenIsString);
                                TokenIsString = false;
                            }
                            TokenName = new StringBuilder();
                            Token = new StringBuilder();
                            if (stack.Count > 0)
                                ctx = stack.Peek();
                            break;

                        case ':':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                                break;
                            }
                            TokenName = Token;
                            Token = new StringBuilder();
                            TokenIsString = false;
                            break;

                        case '"':
                            QuoteMode ^= true;
                            TokenIsString = QuoteMode || TokenIsString;
                            if (QuoteMode == false && Token.Length == 0)
                                Token = new StringBuilder("@#$%EMPTYSTRING");
                            break;

                        case ',':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                                break;
                            }
                            if (Token.Length != 0)
                            {
                                AddElement(ctx, Token.ToString(), TokenName.ToString(), TokenIsString);
                            }
                            TokenName = new StringBuilder();
                            Token = new StringBuilder();
                            TokenIsString = false;
                            break;

                        case '\r':
                        case '\n':
                            break;

                        case ' ':
                        case '\t':
                            if (QuoteMode)
                            {
                                Token.Append(aJSON[i]);
                            }
                            break;

                        case '\\':
                            ++i;
                            if (QuoteMode)
                            {
                                char C = aJSON[i];
                                switch (C)
                                {
                                    case 't':
                                        Token.Append('\t');
                                        break;
                                    case 'r':
                                        Token.Append('\r');
                                        break;
                                    case 'n':
                                        Token.Append('\n');
                                        break;
                                    case 'b':
                                        Token.Append('\b');
                                        break;
                                    case 'f':
                                        Token.Append('\f');
                                        break;
                                    case 'u':
                                        {
                                            string s = aJSON.Substring(i + 1, 4);
                                            Token.Append((char)int.Parse(s, NumberStyles.AllowHexSpecifier));
                                            i += 4;
                                            break;
                                        }
                                    default:
                                        Token.Append(C);
                                        break;
                                }
                            }
                            break;

                        default:
                            Token.Append(aJSON[i]);
                            break;
                    }
                    ++i;
                }
                if (QuoteMode)
                {
                    Debug.LogError("JSON Parse: Quotation marks seems to be messed up.");
                }
                return ctx;
            }

            public virtual void Serialize(System.IO.BinaryWriter aWriter)
            {
            }

            public void SaveToStream(System.IO.Stream aData)
            {
                BinaryWriter W = new System.IO.BinaryWriter(aData);
                Serialize(W);
            }

            public void SaveToFile(string aFileName)
            {
                System.IO.Directory.CreateDirectory((new System.IO.FileInfo(aFileName)).Directory.FullName);
                using (FileStream F = System.IO.File.OpenWrite(aFileName))
                {
                    SaveToStream(F);
                }
            }

            public string SaveToBase64()
            {
                using (MemoryStream stream = new System.IO.MemoryStream())
                {
                    SaveToStream(stream);
                    stream.Position = 0;
                    return System.Convert.ToBase64String(stream.ToArray());
                }
            }

            public static ANode Deserialize(System.IO.BinaryReader aReader)
            {
                BinaryTag type = (BinaryTag) aReader.ReadByte();
                switch (type)
                {
                    case BinaryTag.Array:
                    {
                        int count = aReader.ReadInt32();
                        Array tmp = new Array();
                        for (int i = 0; i < count; i++)
                            tmp.Add(Deserialize(aReader));
                        return tmp;
                    }
                    case BinaryTag.Class:
                    {
                        int count = aReader.ReadInt32();
                        Class tmp = new Class();
                        for (int i = 0; i < count; i++)
                        {
                            string key = aReader.ReadString();
                            var val = Deserialize(aReader);
                            tmp.Add(key, val);
                        }

                        return tmp;
                    }
                    case BinaryTag.Value:
                    {
                        return new Data(aReader.ReadString());
                    }
                    case BinaryTag.IntValue:
                    {
                        return new Data(aReader.ReadInt32());
                    }
                    case BinaryTag.DoubleValue:
                    {
                        return new Data(aReader.ReadDouble());
                    }
                    case BinaryTag.BoolValue:
                    {
                        return new Data(aReader.ReadBoolean());
                    }
                    case BinaryTag.FloatValue:
                    {
                        return new Data(aReader.ReadSingle());
                    }

                    default:
                    {
                        throw new Exception("Error deserializing  Unknown tag: " + type);
                    }
                }
            }

            public static ANode LoadFromStream(System.IO.Stream aData)
            {
                using (BinaryReader R = new System.IO.BinaryReader(aData))
                {
                    return Deserialize(R);
                }
            }

            public static ANode LoadFromFile(string aFileName)
            {
                using (FileStream F = System.IO.File.OpenRead(aFileName))
                {
                    return LoadFromStream(F);
                }
            }

            public static ANode LoadFromBase64(string aBase64)
            {
                var tmp = System.Convert.FromBase64String(aBase64);
                MemoryStream stream = new System.IO.MemoryStream(tmp)
                {
                    Position = 0
                };
                return LoadFromStream(stream);
            }
        }
        // End of JSONNode

        public class Array : ANode, IEnumerable
        {
            private readonly List<ANode> m_List;
            private readonly bool _expand;

            public Array(bool expand = true)
            {
                _expand = expand;
                m_List = new List<ANode>();
            }

            public Array(IEnumerable<ANode> arg, bool expand = true)
                : this(expand)
            {
                m_List.AddRange(arg);
            }

            public Array(IEnumerable<string> arg, bool expand = true)
                : this(expand)
            {
                m_List.AddRange(arg.Select(x => (ANode)new Data(x)));
            }

            public override ANode this[int aIndex]
            {
                get
                {
                    if (aIndex < 0 || aIndex >= m_List.Count)
                        return null;
                    return m_List[aIndex];
                }
                set
                {
                    if (aIndex < 0 || aIndex >= m_List.Count)
                        m_List.Add(value);
                    else
                        m_List[aIndex] = value;
                }
            }

            public override ANode this[string aKey]
            {
                get => null;
                set => m_List.Add(value);
            }

            public override int Count => m_List.Count;

            public override ANode Add(string aKey, ANode aItem, bool allowNulls = true)
            {
                if (aItem == null)
                {
                    if (allowNulls)
                    {
                        aItem = new Data("null");
                    }
                    else
                    {
                        Debug.LogError("JSON.ANode.Add: Nulls are not allowed as values! Key: " + aKey);
                        return this;
                    }
                }
                m_List.Add(aItem);
                return this;
            }

            public override ANode Remove(int aIndex)
            {
                if (aIndex < 0 || aIndex >= m_List.Count)
                    return null;
                ANode tmp = m_List[aIndex];
                m_List.RemoveAt(aIndex);
                return tmp;
            }

            public override ANode Remove(ANode aNode)
            {
                m_List.Remove(aNode);
                return aNode;
            }

            public override IEnumerable<ANode> Children
            {
                get
                {
                    foreach (ANode N in m_List)
                        yield return N;
                }
            }

            public IEnumerator GetEnumerator()
            {
                return m_List.GetEnumerator();
            }

            public override string ToString()
            {
                string result = "[ ";
                foreach (ANode N in m_List)
                {
                    if (result.Length > 2)
                        result += ", ";
                    result += N.ToString();
                }
                result += " ]";
                return result;
            }

            public override string ToString(string aPrefix)
            {
                string result = "[ ";
                foreach (ANode N in m_List)
                {
                    if (result.Length > 3)
                        result += ", ";
                    result += "\n" + aPrefix + "   ";
                    result += N.ToString(aPrefix + "   ");
                }
                result += "\n" + aPrefix + "]";
                return result;
            }

            public override string ToJSON(int prefix = 0)
            {
                string s = new string('\t', prefix + 1);
                string ret = "[ ";
                foreach (ANode n in m_List)
                {
                    if (ret.Length > 3)
                        ret += ", ";
                    ret += (_expand ? "\n" : "") + s;
                    ret += n.ToJSON(prefix + 1);

                }
                ret += (_expand ? "\n" + new string('\t', prefix) : " ") + "]";
                return ret;
            }

            public override void Serialize(System.IO.BinaryWriter aWriter)
            {
                aWriter.Write((byte)BinaryTag.Array);
                aWriter.Write(m_List.Count);
                foreach (ANode t in m_List)
                {
                    t.Serialize(aWriter);
                }
            }
        }
        // End of JSONArray

        public class Class : ANode, IEnumerable
        {
            private readonly Dictionary<string, ANode> _dict;
            private List<string> _keysCache;
            private readonly bool _expand;

            public Class(bool expand = true)
                : base()
            {
                _dict = new Dictionary<string, ANode>();
                _keysCache = new List<string>();
                _expand = expand;
            }

            public override ANode this[string aKey]
            {
                get => _dict.TryGetValue(aKey, out ANode res) ? res : new FakeNode(aKey, this);
                set
                {
                    if (_dict.ContainsKey(aKey))
                        _dict[aKey] = value;
                    else
                    {
                        _dict.Add(aKey, value);
                        _keysCache.Add(aKey);
                    }
                }
            }

            public string GetChildNameAt(int arg)
            {
                return _keysCache[arg];
            }

            public JSON.ANode GetChildAt(int arg)
            {
                return _dict[_keysCache[arg]];
                //var key = _dict.Keys.ToList()[arg];
                //return key == null ? default(JSON.ANode) : _dict[key];
                //return _dict.ElementAt(arg).Value;
            }

            public string[] GetNamesOfAllChildren()
            {
                return _dict.Keys.ToArray();
            }

            public override ANode this[int aIndex]
            {
                get
                {
                    if (aIndex < 0 || aIndex >= _dict.Count)
                        return null;
                    return GetChildAt(aIndex);
                }
                set
                {
                    if (aIndex < 0 || aIndex >= _dict.Count)
                        return;
                    _dict[GetChildNameAt(aIndex)] = value;
                }
            }

            public override int Count => _dict.Count;

            public override ANode Add(string aKey, ANode aItem, bool allowNulls = true)
            {
                if (aItem == null)
                {
                    if (allowNulls)
                    {
                        aItem = new Data("null");
                    }
                    else
                    {
                        Debug.LogError("JSON.ANode.Add: Nulls are not allowed as values! Key: " + aKey);
                        return this;
                    }
                }
                if (!string.IsNullOrEmpty(aKey))
                {
                    if (_dict.ContainsKey(aKey))
                        _dict[aKey] = aItem;
                    else
                    {
                        _dict.Add(aKey, aItem);
                        _keysCache.Add(aKey);
                    }
                }
                else
                {
                    var str = Guid.NewGuid().ToString();
                    _dict.Add(str, aItem);
                    _keysCache.Add(str);
                }
                return this;
            }

            public override ANode Remove(string aKey)
            {
                if (!_dict.ContainsKey(aKey))
                    return null;
                ANode tmp = _dict[aKey];
                _dict.Remove(aKey);
                _keysCache.Remove(aKey);
                return tmp;
            }

            public override ANode Remove(int aIndex)
            {
                return Remove(GetChildNameAt(aIndex));
                /*if (aIndex < 0 || aIndex >= _dict.Count)
                    return null;
                var item = _dict.ElementAt(aIndex);
                _dict.Remove(item.Key);
                return item.Value;*/
            }

            public override ANode Remove(ANode aNode)
            {
                try
                {
                    var item = _dict.First(k => k.Value == aNode);
                    _dict.Remove(item.Key);
                    _keysCache.Remove(item.Key);
                    return aNode;
                }
                catch
                {
                    return null;
                }
            }

            public override IEnumerable<ANode> Children
            {
                get { return _dict.Select(N => N.Value); }
            }

            public IEnumerator GetEnumerator()
            {
                return _dict.GetEnumerator();
            }

            public override string ToString()
            {
                string result = "{";
                foreach (KeyValuePair<string, ANode> N in _dict)
                {
                    if (result.Length > 2)
                        result += ", ";
                    result += "\"" + Escape(N.Key) + "\":" + N.Value.ToString();
                }
                result += "}";
                return result;
            }

            public override string ToString(string aPrefix)
            {
                string result = "{ ";
                foreach (KeyValuePair<string, ANode> N in _dict)
                {
                    if (result.Length > 3)
                        result += ", ";
                    result += "\n" + aPrefix + "   ";
                    result += "\"" + Escape(N.Key) + "\" : " + N.Value.ToString(aPrefix + "   ");
                }
                result += "\n" + aPrefix + "}";
                return result;
            }

            public override string ToJSON(int prefix = 0)
            {
                var s = new StringBuilder(_expand ? new string('\t', prefix + 1) : "");
                var ret = new StringBuilder("{ ");
                //string s = _expand ? new string('\t', prefix + 1) : "";
                //string ret = "{ ";

                foreach (KeyValuePair<string, ANode> n in _dict)
                {
                    if (ret.Length > 3)
                        ret.Append(", ");
                    if (_expand) ret.Append("\n");
                    ret.Append(s.ToString());
                    ret.Append("\"");
                    ret.Append(n.Key);
                    ret.Append("\":");
                    ret.Append(n.Value.ToJSON(prefix + 1));
                }

                if (_expand)
                {
                    ret.Append("\n");
                    ret.Append(new string('\t', prefix));
                }
                ret.Append("}");
                return ret.ToString();
            }

            public override void Serialize(System.IO.BinaryWriter aWriter)
            {
                aWriter.Write((byte)BinaryTag.Class);
                aWriter.Write(_dict.Count);
                foreach (string K in _dict.Keys)
                {
                    aWriter.Write(K);
                    _dict[K].Serialize(aWriter);
                }
            }
        }
        // End of JSONClass

        public class Data : ANode
        {
            private string m_Data;

            public override string Value
            {
                get => m_Data;
                set
                {
                    m_Data = value;
                    Tag = BinaryTag.Value;
                }
            }

            public Data(string aData)
            {
                m_Data = aData;
                Tag = BinaryTag.Value;
            }

            public Data(float aData)
            {
                AsFloat = aData;
            }

            public Data(double aData)
            {
                AsDouble = aData;
            }

            public Data(bool aData)
            {
                AsBool = aData;
            }

            public Data(int aData)
            {
                AsInt = aData;
            }

            public override string ToString()
            {
                return "\"" + Escape(m_Data) + "\"";
            }

            public override string ToString(string aPrefix)
            {
                return "\"" + Escape(m_Data) + "\"";
            }

            public override string ToJSON(int prefix = 0)
            {
                switch (Tag)
                {
                    case BinaryTag.DoubleValue:
                    case BinaryTag.FloatValue:
                    case BinaryTag.IntValue:
                    case BinaryTag.BoolValue:
                        return m_Data;
                    case BinaryTag.Value:
                        return string.Format("\"{0}\"", Escape(m_Data));
                    default:
                        throw new NotSupportedException("This shouldn't be here: " + Tag.ToString());
                }
            }

            public override void Serialize(System.IO.BinaryWriter aWriter)
            {
                Data tmp = new Data("");

                tmp.AsInt = AsInt;
                if (tmp.m_Data == this.m_Data)
                {
                    aWriter.Write((byte)BinaryTag.IntValue);
                    aWriter.Write(AsInt);
                    return;
                }
                tmp.AsFloat = AsFloat;
                if (tmp.m_Data == this.m_Data)
                {
                    aWriter.Write((byte)BinaryTag.FloatValue);
                    aWriter.Write(AsFloat);
                    return;
                }
                tmp.AsDouble = AsDouble;
                if (tmp.m_Data == this.m_Data)
                {
                    aWriter.Write((byte)BinaryTag.DoubleValue);
                    aWriter.Write(AsDouble);
                    return;
                }

                tmp.AsBool = AsBool;
                if (tmp.m_Data == this.m_Data)
                {
                    aWriter.Write((byte)BinaryTag.BoolValue);
                    aWriter.Write(AsBool);
                    return;
                }
                aWriter.Write((byte)BinaryTag.Value);
                aWriter.Write(m_Data);
            }
        }
        // End of JSONData

        internal class FakeNode : ANode
        {
            private readonly string _key;
            private readonly ANode _node;

            public FakeNode(string key, ANode node)
            {
                _key = key;
                _node = node;
            }

            private void LogError()
            {
                Debug.LogError("JSON: Key \"" + _key + "\" not found in: " + _node.ToJSON());
            }

            public override string Value
            {
                get
                {
                    LogError();
                    return "";
                }
                set => LogError();
            }

            public override string ToJSON(int prefix = 0)
            {
                return "";
            }

            public override Array AsArray
            {
                get
                {
                    LogError();
                    return null;
                }
            }

            public override Class AsObject
            {
                get
                {
                    LogError();
                    return null;
                }
            }
        }
    }