﻿using UnityEngine;

    /// <summary>
    /// Синглетон MonoBehaviour. Автосоздаётся при обращении.
    /// </summary>
    /// <typeparam name="T">Тип синглетона</typeparam>
public abstract class ASingleton_MB<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T _instance;

    /// <summary>
    /// Используется для того, чтобы узнать, существует ли instance синглетона. Нужен поскольку проверка через Instance != null вызовет автосоздание синглетона.
    /// </summary>
    public static bool InstanceExists => _instance != null;

    public static T Instance
	{
		get
		{
            if (_instance != null)
            {
                return _instance;
            }
            
            GameObject singleton = new GameObject();
            singleton.AddComponent<T>();
            singleton.name = typeof(T) + " (singleton)";

            return _instance;
		}
	}

    protected virtual void Awake()
    {
        if (_instance != null)
        {
            Debug.LogError("ASingleton_MB<" + typeof(T) + ">: Duplicate singleton!", gameObject);
        }
        _instance = this as T;
    }

    protected virtual void OnDestroy()
    {
        _instance = null;
    }
}
